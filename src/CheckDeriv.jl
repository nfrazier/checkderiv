


module CheckDeriv
using Calculus, Printf

export check_deriv, get_numerical

function check_deriv(x::Vector,f_in::Function; tol = 1e-6)

  # Wrapper for value only
  function fvalue(x::Union{Float64,Array{Float64}})
      out = f_in(x,[])
  end

  # wrapper for gradient only
  function gvalue(x::Union{Float64,Array})
      grad = zeros(Float64,length(x))
      f_in(x,grad)
      return grad
  end

  ga = gvalue(x)   # analytical
  auto_g = Calculus.gradient(fvalue) # will use Calculus.gradient
  gn = auto_g(x)            # calculate numerical
  max_diff = maximum(abs.(ga - gn))
  diff = ga - gn
  nn = [1:length(x);]

  if max_diff > tol
    warn("Analytic derivative fails. abs(diff) = ", max_diff, "\n")
    warn("Largest difference for parameter(s) ", findin(abs.(diff),max_diff),"\n")
    @printf("%5s |  %10s   %10s ---     %10s\n","Pnum", "Analytic", "Numerical","Difference")
    map(jj -> @printf(
        "%5d | %10.2f   %10.2f  ---  %10.2f \n",
        nn[jj],
        ga[jj],
        gn[jj],
        diff[jj]),
        nn
        );
  else
    println("Analytic derivative passed at: ", max_diff, "\n")
  end
  return Dict("max_diff"=>max_diff,"analytical"=>ga,"numerical"=>gn)
end

function get_numerical(x::Vector,f_in::Function)
  # Wrapper for value only
  function fvalue(x::Union{Float64,Array{Float64}})
      out = f_in(x,[])
  end

  auto_g = Calculus.gradient(fvalue) # will use Calculus.gradient
  gn = auto_g(x)            # calculate numerical
end

end # end module
